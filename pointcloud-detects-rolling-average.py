import numpy as np
from numpy.random import normal
import time
import random
from math import sqrt

from kabsch import rigid_transform_3D

lastFrame = None
# From: https://docs.google.com/document/d/1F4Y6S6KtZ4f8RBE4W-o9x6xVXbqsw8UIGWPkML-on1Y/edit
linearErrorTrend = 0.0151 # increase in proportion error per each meter true depth increase
velocity = 10 # z velocity in meters per second
framerate = 60 # reciprocal of time (in seconds) between each frame
timestep = 1.0 / framerate

def main():
    while(True):
        frame = get_frame()
        print(f"framePos: {frame.framePos[0]}, {frame.framePos[1]}, {frame.framePos[2]}")
        print(frame.points)
        time.sleep(1)


def get_frame():
    return dummy_get_frame()

def introduceError(realz):
    global linearErrorTrend
    error = linearErrorTrend * realz
    z = 0.0
    while(z <= 0.0):
        z = realz + error * realz * normal(loc=0, scale=0.5, size=None) # not sure how good this math is
    return z

def get_rand_point(framePos = (0.0, 0.0, 0.0)):
    realx = random.random() * 20.0
    realy = random.random() * 20.0
    realz = random.random() * 20.0

    fakez = introduceError(realz)

    outP = (realx, realy, fakez)
    outP = tuple(map(sum, zip(outP, framePos)))
    return (outP, framePos[2] + realz)

def dummy_get_frame():

    global lastFrame
    global linearErrorTrend
    global velocity
    global framerate
    global timestep

    if lastFrame is None:
        randPoints = []
        for i in range(10):
            randPoints.append(get_rand_point())
        lastFrame = Frame(randPoints)
        lastFrame.framePos = (0.0, 0.0, 0.0)
    else:
        travel_dist = timestep * velocity
        newFramePoints = []
        newFramePos = (lastFrame.framePos[0], lastFrame.framePos[1], lastFrame.framePos[2] + travel_dist)
        for aPoint in lastFrame.points:
            realx, realy = aPoint[0][0], aPoint[0][1]
            realz = aPoint[1]
            if (realz - newFramePos[2] <= 0): # replace point if it is behind the camera
                p = get_rand_point(newFramePos)
                newFramePoints.append(p)
            else:
                fakez = introduceError(realz - newFramePos[2]) + newFramePos[2]
                newFramePoints.append(((realx, realy, fakez), realz))
        lastFrame = Frame(newFramePoints)
        lastFrame.framePos = newFramePos

    return lastFrame

# end dummy_get_frame()


class Frame:
    framePos = (0.0,0.0,0.0)

    def __init__(self):
        self.numPoints = 0
        self.points = []

    def __init__(self, pointsIn):
        self.numPoints = len(pointsIn)
        self.points = pointsIn.copy()

main()
