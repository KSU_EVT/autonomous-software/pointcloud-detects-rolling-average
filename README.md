# pointcloud-detects-rolling average
An experimental technique to get a rolling average of detected cartesian points using frames n, n-1, and n-2

# Premise:
Take 3 frames worth of cone XYZ points from the camera. Henceforth I will refer to these as frames `a`, `b`, and `c`.


For each cone detected in `b`, we will cull points and only use those for which a cone in `a` and a cone in `c` which are dist < (V\*T\*M) from a cone in `b`


Where V is kart velocity in m/s, T is the time in seconds between each frame, and M is an arbitrary constant multiplier
Next we group each cone in `b` with its counterpart in `a` and `c` (limited to only those where such counterparts exist). The average of all ((b-a) + (c-b)) / 2 will give us an accurate estimate of the meters of translation and rotation of the camera from frame `a` -> `b` and `b` -> `c`


Next we make a copy of all cones in `a`, and translate them by the value determined in the previous step so they map closely to `b`. We do the same thing with `c`, translating them in the opposite direction so they also map onto `b`
Then for every triple XYZ representing a translated cone in `a`, a cone in `b`, and a translated cone in `c`, Take the average of their XYZ values to define a point `P`


Assume the depth error follows a normal distribution. `P`, the mean location from translated a, b, and reverse-translated c will be much closer to the true location than any single frame itself. Latency reported to SLAM is only increased by (1 / framerate). At 60fps for example, this is only 17 milliseconds.
